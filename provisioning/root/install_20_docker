#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail

install_docker() {
  apt-get -y install \
          apt-transport-https \
          ca-certificates \
          curl \
          gnupg-agent \
          software-properties-common
  [[ -z "$(apt-key fingerprint 0EBFCD88)" ]] && \
      curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
  add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/debian \
      $(lsb_release -cs) \
      stable"
  apt-get update
  apt-get -y install docker-ce docker-ce-cli containerd.io
  usermod -aG docker vagrant
}


install_docker-compose () {
  local dc_inst="/usr/local/bin/docker-compose"

  [[ -f "${dc_inst}" ]] && return 0
  curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o "${dc_inst}"
  chmod +x "${dc_inst}"
}

install_docker
install_docker-compose
