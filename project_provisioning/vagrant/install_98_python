#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail

readonly GLOBAL_PYTHON_VERSION='3.8.2'

readonly DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# shellcheck disable=SC2016
install_pyenv() {
  echo "Installing pyenv"
  case "$(uname)" in
    Darwin*)
      pkg_install pyenv
      # FIXME: Set up PYENV_ROOT
      ;;
    *)
      [[ -d "${HOME}/.pyenv" ]] || git clone https://github.com/pyenv/pyenv.git ~/.pyenv
      sed -i --follow-symlinks \
        -e '/^export PYENV_ROOT="\${HOME}\/.pyenv"$/ { :l;n;bl }' \
        -e '$aexport PYENV_ROOT="${HOME}/.pyenv"' "${HOME}/.bash_profile"
      sed -i --follow-symlinks \
        -e '/^export PATH="\${PYENV_ROOT}\/bin:\${PATH}"/ { :l;n;bl }' \
        -e '$aexport PATH="${PYENV_ROOT}/bin:${PATH}"' "${HOME}/.bash_profile"
      sed -i --follow-symlinks \
        -e '/eval "\$(pyenv init -)"/ { d }' \
        -e '$acommand -v pyenv > /dev/null 2>&1 && eval "\$(pyenv init -)" ; true' "${HOME}/.bash_profile"
      ;;
  esac
}

build_and_install_global_python() {
  echo "Building and installing global python version: ${GLOBAL_PYTHON_VERSION}"
  # shellcheck source=/dev/null
  . ~/.bash_profile   # Load new pyenv settings (see install_pyenv())
  pyenv versions | grep "${GLOBAL_PYTHON_VERSION}" || pyenv install "${GLOBAL_PYTHON_VERSION}"
  pyenv global "${GLOBAL_PYTHON_VERSION}"
}


# shellcheck disable=SC2016
install_poetry() {
  echo "Installing poetry"
  [[ -x "$(command -v poetry)" ]] \
    || { 
      curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python ; 
    }
  sed -i --follow-symlinks \
    -e '/^source "\${HOME}\/.poetry\/env"$/ { :l;n;bl }' \
    -e '$asource "${HOME}/.poetry/env"' "${HOME}/.bashrc"
}

install_pyenv
build_and_install_global_python
install_poetry

exit 0
